-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 02, 2020 at 12:11 AM
-- Server version: 5.6.36-82.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reviewgr_reviewgrowth`
--

-- --------------------------------------------------------

--
-- Table structure for table `businesslocations`
--

CREATE TABLE `businesslocations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `find_business_location` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `business_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `business_address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `business_rating` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `business_page_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `lat` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `lng` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `business_logo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `business_review_link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `facebook_link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `twitter_link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `linkedin_link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `instagram_link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `client_satisfaction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  `isdeleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `lrvstatus` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(191) DEFAULT NULL,
  `updated_by` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `businesslocations`
--

INSERT INTO `businesslocations` (`id`, `user_id`, `find_business_location`, `business_name`, `business_address`, `business_rating`, `business_page_id`, `lat`, `lng`, `phone`, `business_logo`, `business_review_link`, `facebook_link`, `twitter_link`, `linkedin_link`, `instagram_link`, `client_satisfaction`, `status`, `isdeleted`, `lrvstatus`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'Binary Data Private Limited : Best Web Development & SEO Company Mohali, Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Punjab, India', 'Binary Data Private Limited : Best Web Development & SEO Company Mohali', 'Plot No. E, 57, Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Punjab 160071, India', '', 'ChIJ480zVfTuDzkRmuggkae-kmQ', '30.7093087', '76.7076014', '+19874563210', '', 'binarydata', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.linkdein.com', 'https://www.instagram.com', '1', '1', '0', 1, '2', '2', '2019-12-18 17:06:35', '2019-12-18 17:07:20'),
(2, 3, 'Advantage Web Services, Avenue P, Brooklyn, NY, USA', 'Advantage Web Services', '1701 Avenue P Suite M, Brooklyn, NY 11229, United States', '', 'ChIJpa0iIilbwokRlGAmFmagZ38', '40.610855', '-73.956535', '+13478935116', '', 'advantagewebservices', 'https://facebook.com/advantagewebservices', '', '', '', '1', '1', '0', 0, '3', '3', '2019-12-18 21:34:22', '2019-12-18 21:35:26'),
(3, 8, 'Sahibzada Ajit Singh Nagar, Punjab 160071, India', 'Sahibzada Ajit Singh Nagar, Punjab 160071', 'Sahibzada Ajit Singh Nagar, Punjab 160071, India', '', 'ChIJFyk8mJLuDzkR5-LS1zAPnOw', '30.7038528', '76.7076768', '+123456789', '', 'reviewit', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.linkedin.com', 'https://www.instagram.com', '0', '1', '0', 0, '8', '8', '2019-12-24 13:00:31', '2019-12-24 13:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `businesssetting`
--

CREATE TABLE `businesssetting` (
  `id` int(10) UNSIGNED NOT NULL,
  `business_name` varchar(191) DEFAULT NULL,
  `business_address` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `website` varchar(191) DEFAULT NULL,
  `logo` varchar(191) DEFAULT NULL,
  `facebook_link` varchar(191) DEFAULT NULL,
  `twitter_link` varchar(191) DEFAULT NULL,
  `instagram_link` varchar(191) DEFAULT NULL,
  `linkedin_link` varchar(191) DEFAULT NULL,
  `google_link` varchar(191) DEFAULT NULL,
  `default_currency` varchar(191) DEFAULT 'USD',
  `language` varchar(191) DEFAULT 'English',
  `payment_mode` varchar(191) DEFAULT NULL,
  `paypal_test_email` varchar(191) DEFAULT NULL,
  `paypal_live_email` varchar(191) DEFAULT NULL,
  `stripe_test_pub_key` varchar(191) DEFAULT NULL,
  `stripe_test_sec_key` varchar(191) DEFAULT NULL,
  `stripe_live_pub_key` varchar(191) DEFAULT NULL,
  `stripe_live_sec_key` varchar(191) DEFAULT NULL,
  `google_map_url` varchar(191) DEFAULT NULL,
  `ex1` varchar(191) DEFAULT NULL,
  `ex2` varchar(191) DEFAULT NULL,
  `ex3` varchar(191) DEFAULT NULL,
  `ex4` varchar(191) DEFAULT NULL,
  `ex5` varchar(191) DEFAULT NULL,
  `ex6` varchar(191) DEFAULT NULL,
  `ex7` varchar(191) DEFAULT NULL,
  `ex8` varchar(191) DEFAULT NULL,
  `ex9` varchar(191) DEFAULT NULL,
  `ex10` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `businesssetting`
--

INSERT INTO `businesssetting` (`id`, `business_name`, `business_address`, `email`, `phone`, `website`, `logo`, `facebook_link`, `twitter_link`, `instagram_link`, `linkedin_link`, `google_link`, `default_currency`, `language`, `payment_mode`, `paypal_test_email`, `paypal_live_email`, `stripe_test_pub_key`, `stripe_test_sec_key`, `stripe_live_pub_key`, `stripe_live_sec_key`, `google_map_url`, `ex1`, `ex2`, `ex3`, `ex4`, `ex5`, `ex6`, `ex7`, `ex8`, `ex9`, `ex10`, `created_at`, `updated_at`) VALUES
(1, 'ReviewGrowth', 'USA', 'reviewgrowth@gmail.com', '9874563210', 'https://reviewgrowth.com/', '1553690402.png', 'https://www.facebook.com/reviewgrowth', 'https://www.twitter.com/reviewgrowth', 'https://www.instagram.com/reviewgrowth', 'https://www.linkedin.com/reviewgrowth', 'https://www.google.com/', 'USD', 'english', 'Test', 'test@email.com', '', '123112232', '123112232', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '2019-04-30 12:02:24');

-- --------------------------------------------------------

--
-- Table structure for table `business_reviews`
--

CREATE TABLE `business_reviews` (
  `id` int(11) NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `reviewer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `reviewer_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `reviewer_rating` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `reviewer_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `business_user_id` int(11) DEFAULT NULL,
  `business_place_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `business_slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `is_complete` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Active,0=Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `errorlog`
--

CREATE TABLE `errorlog` (
  `id` int(10) UNSIGNED NOT NULL,
  `error_message` longtext,
  `line_number` varchar(191) DEFAULT NULL,
  `file_name` varchar(191) DEFAULT NULL,
  `browser` varchar(191) DEFAULT NULL,
  `operating_system` varchar(191) DEFAULT NULL,
  `loggedin_id` int(11) DEFAULT NULL,
  `ip_address` varchar(191) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Deactive',
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `live_reviews`
--

CREATE TABLE `live_reviews` (
  `id` int(11) NOT NULL,
  `live_review_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `live_review_reviewer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `live_review_datetime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `live_review_rating` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `live_review_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `live_review_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `live_review_language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_user_id` int(11) DEFAULT NULL,
  `business_place_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `is_complete` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Active,0=Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `live_reviews`
--

INSERT INTO `live_reviews` (`id`, `live_review_id`, `live_review_reviewer`, `live_review_datetime`, `live_review_rating`, `live_review_text`, `live_review_url`, `live_review_language`, `business_user_id`, `business_place_id`, `business_slug`, `status`, `is_deleted`, `is_complete`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IHSgJ', 'Binarydata 2014', '2016-03-15T15:12:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s113839751057893343988!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(2, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IGigI', 'James Williamson', '2016-04-26T09:57:00Z', '5', 'Binary Data upgraded and redesigned my business website and I have nothing but great  things to say about the whole process with this company. Since the first day the website was published I have been receiving so many complements.\n\nBinary Data provides technical expertise, efficiency and creativity in whatever quantity you need them. The employees are slick, helpful, and deliver the goods how you want them, when you want them!', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s105745797221151490768!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(3, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IFygH', 'Jessica Taylor', '2016-09-13T11:39:00Z', '5', 'Binary Data did a great job building my website! They are friendly and great to work with. They were worth every penny and I recommend him to anyone looking for help creating a website.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s116527525169491364890!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(4, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IFCgG', 'priyanka sharma', '2017-09-27T15:21:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s104324697030675941317!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(5, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IESgF', 'David Parker', '2017-10-07T10:19:00Z', '5', 'Team are extremely creative and professional and get things done right the first time. I can not emphasize enough what a smooth process it was to get our dream website. Wonderful team to work with! Very prompt and structured processes and they really go the extra mile for client satisfaction.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s113696320598163562150!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(6, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IDigE', 'Abhishek Katoch', '2018-01-06T17:43:00Z', '4', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s108235647915430686243!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(7, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4ICygD', 'Aman1313', '2018-01-08T07:29:00Z', '4', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s103732288512223487953!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(8, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4ICCgC', 'Sandeep Singh', '2018-01-12T13:40:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s111590388395420955965!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(9, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IBSgB', 'Pirtpal Singh', '2018-02-28T14:24:00Z', '5', 'Good', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s104345349059807843178!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'so', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(10, '0ahUKEwjy34qjh7_mAhUOS6wKHZb1DiUQ4R4IAigA', 'Smith Kashyap', '2018-03-16T14:56:00Z', '3', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s108315280406653171109!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(11, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IHSgJ', 'Anshuman Thakur', '2018-03-25T09:06:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s111605439015116064832!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(12, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IGigI', 'Amy Wyatt', '2018-04-05T14:59:00Z', '5', 'I recently got a website done from BINARY DATA for my business, and the quality of my new website is fabulous. HIGHLY RECOMMENDED.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s106358325071182861398!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(13, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IFygH', 'Raghav Thakur', '2018-04-21T14:24:00Z', '5', 'One of my friend is working here for many years. He told me about the company\'s environment . After listened him i  tried  here as a PHP Developer. Fortunately, i was selected. Still i cannot think about to change the company because they guys really cares for their team members. Today i have completed my 3 years with this company. And i am very happy to work here.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s105429906279221241378!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(14, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IFCgG', 'richard simmons', '2018-04-21T14:45:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s113256752428882642738!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(15, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IESgF', 'pankaj kumar', '2018-04-21T14:56:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s101692793184474174971!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(16, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IDigE', 'Joseph william', '2018-05-08T12:44:00Z', '5', 'Binary Data team is very helpful and understanding.They have always sorted out every issue of my site. I really appreciate their help and suggestions in every manner for website maintenance. HIGHLY RECOMMENDED.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s109715879238477649624!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(17, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4ICygD', 'Margaret Thatcher Thatcher', '2018-05-08T13:12:00Z', '5', 'I have worked with binary data on several projects.They have extremely bright, knowledgeable and involved.They have truly dedicated towards their works. I am very happy with my new website, all thanks to  binary  data  and her team.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s116883581352714226165!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(18, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4ICCgC', 'Sophie Taylor', '2018-05-11T14:40:00Z', '5', 'I worked with Binary Data for my e commerce website development. They were extremely helpful and thorough in their work. I appreciate their hard work and open communication also their daily updates.I would recommend binary data private limited to anyone who needs help in getting their website successfully done.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s109962530765481161572!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(19, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IBSgB', 'shivam arora', '2018-05-30T09:43:00Z', '5', 'Best company', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s114216240714100983503!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'ca', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(20, '0ahUKEwjf_vSjh7_mAhVLVK0KHTrzBYgQ4R4IAigA', 'Ashish Kumar', '2018-06-14T11:13:00Z', '5', 'nice company', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s113536459920544636925!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'ca', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(21, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IHSgJ', 'Faddey Clark', '2018-07-06T13:37:00Z', '5', 'We used Binary Data to design and launch our small business website. Jagmohan and Shakti were great organised and responsive at every step of the process. We truly feel like one team working towards one common goal. We would highly recommend them to anyone looking for similar services.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s102444802353816849139!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(22, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IGigI', 'Tad Miller', '2018-07-13T14:33:00Z', '5', 'I tried to do the website myself. It took over two months and the initial versions are did not look professional enough. I  immediately contact  binary data. They have great taste. The website they have created look stylish and professional. I would recommend binary data to everyone who need to build professional website.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s110235161526413428529!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(23, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IFygH', 'Neeraj Kumar', '2018-07-20T08:29:00Z', '1', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s114667441311085410337!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(24, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IFCgG', 'surinder singh', '2018-07-24T10:16:00Z', '4', 'Okay', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s103923129857007869386!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'tl', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(25, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IESgF', 'surinder singh timpa', '2018-08-11T06:19:00Z', '3', 'Okay', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s115166632486981537838!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'tl', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(26, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IDigE', 'kavita singh', '2018-08-14T11:27:00Z', '3', 'Okay', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s113691227803945734480!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'tl', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(27, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4ICygD', 'surinder singh', '2018-09-03T22:29:00Z', '4', 'Nice', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s114443899915982546584!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'ro', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(28, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4ICCgC', 'sumeet kumar', '2018-09-25T09:16:00Z', '5', 'This is the Best place to learn corporate culture. Seniors are supportive. The Culture is very positive. Overall its great to be a part of Binary Data Team !!', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s108004355665315541821!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(29, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IBSgB', 'Ankita Garg', '2018-09-25T09:24:00Z', '5', 'Great organisation to learn latest technologies and  provide a good working environment.Seniors are very friendly and intelligent.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s100433204937974889422!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(30, '0ahUKEwjv7Lykh7_mAhUOKK0KHZtlA18Q4R4IAigA', 'Shabnam Chandel', '2018-10-01T10:51:00Z', '4', 'Binary data Pvt Ltd maintains an exceptional level in web designing, digital marketing and web development services. They possess the potential to give you the best quality work as per your needs. Extremely pleased to work with the development team members!', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s100852672011597362056!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(31, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IHSgJ', 'javed ahmed', '2018-10-17T10:58:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s106136562395973474536!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(32, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IGigI', 'Chudail Ka Saaya', '2018-10-24T09:11:00Z', '5', 'Providing Best IT Services', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s111598317296528675941!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(33, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IFygH', 'Nitin namdev', '2018-11-13T07:43:00Z', '5', 'Nice Company. All the staff very humble and supporting.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s112177496696533952536!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(34, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IFCgG', 'Bharat Raj Swami', '2018-11-13T07:52:00Z', '5', 'One of the best IT Company (web designing and development, digital marketing, Graphics design etc.) In mohali, chandigarh area. Working with worldwide clients.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s104908148438015787324!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(35, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IESgF', 'poonam Sankhyan', '2018-11-20T09:23:00Z', '5', 'I found binary data pvt ltd a very professional  company. Management is highly supportive and this is a company  where any fresher can learn to improve  upon their skills.There are many  examples  who started their career from this company.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s118147846650061274608!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(36, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IDigE', 'Gagan Singh', '2018-11-20T11:03:00Z', '3', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s100655256211378361634!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(37, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4ICygD', 'Shekhar Mehr', '2018-11-20T11:49:00Z', '5', 'Binary Data Pvt Ltd is best web designer &  developer ,app developer, and also digital marketing service provider. Having  excellent supporting staff.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s116714982329876760788!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(38, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4ICCgC', 'Anjna Sharma', '2018-11-23T16:09:00Z', '5', 'Binary data is a wonderful company. I got best troubleshooting for my shopify website and I would like to recommend everyone for best IT services.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s112802355120890295394!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(39, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IBSgB', 'Sumeet kumar', '2018-11-30T13:59:00Z', '5', 'Nice', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s101805404682869684769!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'ro', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(40, '0ahUKEwi0w6ilh7_mAhUF7qwKHeirAXoQ4R4IAigA', 'Pooja Singh', '2018-12-17T15:46:00Z', '5', 'Awesome', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s104455600061968675174!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'af', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(41, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IICgJ', 'SHASHI RANA', '2018-12-26T17:24:00Z', '5', 'It was an excellent experience working with Binary Data ,\nBinary data has assisted me in building an entirely new website. Always replies quickly to questions & makes himself available in case of emergencies.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s101990293839956740427!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(42, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IHSgI', 'Shilpa Salehia', '2019-01-12T09:41:00Z', '5', 'Awesome! Skills of web development & other IT services. best part of Binary Data professional, very responsive to client needs.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s108554797443523636934!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(43, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IGigH', 'KRITIKA SHARMA', '2019-01-12T09:49:00Z', '5', 'Binary Data is Best software development company. I must say it is a perfect place to work. Company will  provide you with great opportunities to grab good skills and improve working ability.  Here working is like living your passion!!', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s103229184152673268031!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(44, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IFygG', 'shilpa rana', '2019-01-31T09:57:00Z', '5', 'Awesome team and management.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s100294788417223987175!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(45, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IESgF', 'Aman Choudhary', '2019-02-02T14:26:00Z', '5', 'I worked at Binary Data Pvt Ltd for more than 1 year and this is my first company joined as fresher. I didn’t find any issue with the company and I think it is the excellent place to work where you are given good exposure to learn many things. Management is awesome and they always motivate the employees.Keep it up', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s113601354408031333498!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(46, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IDigE', 'Bandana Harsh Yashu', '2019-02-06T17:06:00Z', '5', 'Binary Data is a nice company. All the team members are very nice they always help each other. Team members treat this company as their own company. Dedicated work on projects and give their best to make the project successful. Nice place to grow as professional.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s112681469622505193219!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(47, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4ICygD', 'ravneet kaur', '2019-05-03T14:58:00Z', '5', 'I haven\'t join the company due to my personal issues. But today, during my interview i have seen that the staff of the company is very humble. HR and mam who took my technical interview both were very nice. Thanks', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s113848557092287569910!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(48, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4ICCgC', 'Achin Sharma', '2019-06-24T15:09:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s110518738872262574469!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(49, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IBSgB', 'Isha Devi', '2019-07-04T08:56:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s102930988265488454179!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(50, '0ahUKEwiO--Slh7_mAhUBWqwKHZrLC3QQ4R4IAigA', 'shashi rana', '2019-07-04T09:05:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s106256412684704211562!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(51, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IHSgJ', 'Marlon Andres', '2019-07-13T06:58:00Z', '5', 'My experience with Binary Data has been great. We are fully satisfied with a work they delivered to us!. I must suggest Binary Data Pvt Ltd to everyone for IT services, it is a best website development company in mohali', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s104594906520538075892!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(52, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IGigI', 'Leafy Pros', '2019-07-13T07:21:00Z', '5', 'If you are looking best SEO company in Mohali, India. I highly recommend Binary Data Pvt Ltd. We have tried many SEO and digital marketing companies in the past two years to establish \"Leafy Pros\" as a brand in the USA, but there is no result, then contact Binary Data company. great SEO team has good knowledge give fast results on my websites.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s100800766340920786782!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(53, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IFygH', 'Amit Laroiya', '2019-07-15T12:03:00Z', '4', 'Software', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s103263833716283140516!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(54, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IFCgG', 'sandeep thakur', '2019-07-26T15:00:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s111812367188947941048!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(55, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IESgF', 'Sumit Kumar', '2019-08-02T09:50:00Z', '5', 'Good working environment.. Management is very cooperative.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s110476404705737437595!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(56, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IDigE', 'Manisha Thakur', '2019-09-03T10:00:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s107090750760351090081!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(57, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4ICygD', 'Dev2 BDPL', '2019-09-30T07:53:00Z', '5', 'My experience with Binary Data has been great. We are fully satisfied with a work they delivered to us!', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s110463406230821487081!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(58, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4ICCgC', 'Nahid Creation', '2019-10-18T09:34:00Z', '4', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s109985579187271072225!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(59, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IBSgB', 'Dev1 BDPL', '2019-10-22T10:52:00Z', '5', 'Very nice company', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s105563762238734803492!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', 'en', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL),
(60, '0ahUKEwit_KKmh7_mAhURXa0KHf8MD60Q4R4IAigA', 'Rajeev Goswami', '2019-12-05T11:35:00Z', '5', 'No review was written for this submission.', 'https://www.google.com/maps/reviews/data=!4m5!14m4!1m3!1m2!1s101316789746071267465!2s0x390feef45533cde3:0x6492bea79120e89a?hl=en-US', '', 2, 'ChIJ480zVfTuDzkRmuggkae-kmQ', 'binarydata', '1', '0', '0', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loginhistory`
--

CREATE TABLE `loginhistory` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `login_time` timestamp NULL DEFAULT NULL,
  `logout_time` timestamp NULL DEFAULT NULL,
  `browser` varchar(191) DEFAULT NULL,
  `operating_system` varchar(191) DEFAULT NULL,
  `ip_address` varchar(191) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Deactive',
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loginhistory`
--

INSERT INTO `loginhistory` (`id`, `user_id`, `login_time`, `logout_time`, `browser`, `operating_system`, `ip_address`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 2, '2019-12-18 17:05:42', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058163231', '1', '0', '2019-12-18 17:05:42', '2019-12-18 17:05:42'),
(2, 2, '2019-12-18 17:09:43', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058163226', '1', '0', '2019-12-18 17:09:43', '2019-12-18 17:09:43'),
(3, 3, '2019-12-18 21:33:47', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1814637077', '1', '0', '2019-12-18 21:33:47', '2019-12-18 21:33:47'),
(4, 4, '2019-12-18 22:14:32', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2015186010', '1', '0', '2019-12-18 22:14:32', '2019-12-18 22:14:32'),
(5, 2, '2019-12-20 10:08:28', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '456694972', '1', '0', '2019-12-20 10:08:28', '2019-12-20 10:08:28'),
(6, 2, '2019-12-20 10:10:23', NULL, 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '456694972', '1', '0', '2019-12-20 10:10:23', '2019-12-20 10:10:23'),
(7, 1, '2019-12-21 11:18:56', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1860292171', '1', '0', '2019-12-21 11:18:56', '2019-12-21 11:18:56'),
(8, 1, '2019-12-21 11:20:47', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1860292171', '1', '0', '2019-12-21 11:20:47', '2019-12-21 11:20:47'),
(9, 1, '2019-12-21 11:22:14', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1860292171', '1', '0', '2019-12-21 11:22:14', '2019-12-21 11:22:14'),
(10, 1, '2019-12-24 09:58:03', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 09:58:03', '2019-12-24 09:58:03'),
(11, 1, '2019-12-24 11:17:51', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 11:17:51', '2019-12-24 11:17:51'),
(12, 2, '2019-12-24 11:27:35', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 11:27:35', '2019-12-24 11:27:35'),
(13, 1, '2019-12-24 11:29:12', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 11:29:12', '2019-12-24 11:29:12'),
(14, 5, '2019-12-24 11:49:35', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 11:49:35', '2019-12-24 11:49:35'),
(15, 6, '2019-12-24 11:56:33', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 11:56:33', '2019-12-24 11:56:33'),
(16, 7, '2019-12-24 12:05:35', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 12:05:35', '2019-12-24 12:05:35'),
(17, 1, '2019-12-24 12:15:29', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 12:15:29', '2019-12-24 12:15:29'),
(18, 1, '2019-12-24 12:17:09', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 12:17:09', '2019-12-24 12:17:09'),
(19, 2, '2019-12-24 12:19:50', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 12:19:50', '2019-12-24 12:19:50'),
(20, 2, '2019-12-24 12:25:17', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 12:25:17', '2019-12-24 12:25:17'),
(21, 8, '2019-12-24 12:58:27', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 12:58:27', '2019-12-24 12:58:27'),
(22, 2, '2019-12-24 13:15:39', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 13:15:39', '2019-12-24 13:15:39'),
(23, 1, '2019-12-24 13:17:12', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 13:17:12', '2019-12-24 13:17:12'),
(24, 2, '2019-12-24 13:28:34', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058216111', '1', '0', '2019-12-24 13:28:34', '2019-12-24 13:28:34'),
(25, 2, '2019-12-24 13:32:34', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 13:32:34', '2019-12-24 13:32:34'),
(26, 1, '2019-12-24 13:59:36', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 13:59:36', '2019-12-24 13:59:36'),
(27, 2, '2019-12-24 14:00:20', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '1969285908', '1', '0', '2019-12-24 14:00:20', '2019-12-24 14:00:20'),
(28, 8, '2020-01-01 16:02:28', NULL, 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', '2058213201', '1', '0', '2020-01-01 16:02:28', '2020-01-01 16:02:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_03_01_042307_create_users_table', 1),
(3, '2019_03_01_044509_create_post_table', 1),
(4, '2019_03_01_045941_create_security_question_table', 1),
(5, '2019_03_01_050741_create_errorlog_table', 1),
(6, '2019_03_01_051934_create_business_setting_table', 1),
(7, '2019_03_06_103344_add_fields_to_users', 2),
(8, '2019_03_07_040847_create_chat_table', 3),
(9, '2019_03_07_093846_create_friends_table', 3),
(10, '2019_03_08_052513_create_message_table', 4),
(11, '2019_03_08_060825_create_attachments_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) DEFAULT NULL,
  `description` text,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Deactive',
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `created_by` varchar(191) DEFAULT NULL,
  `updated_by` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `description`, `status`, `deleted`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin desc', '1', '0', '1', '1', '2019-03-10 22:34:06', '2019-03-18 03:28:07'),
(2, 'Business owner', 'Business owner desc', '1', '0', '1', '1', '2019-03-11 06:56:22', '2019-03-16 07:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `can_add` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=yes,0=no',
  `can_edit` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `can_delete` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `can_view` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=yes,0=no',
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Deactive',
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `created_by` varchar(191) DEFAULT NULL,
  `updated_by` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `section_id`, `can_add`, `can_edit`, `can_delete`, `can_view`, `status`, `deleted`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '1', '1', '1', '1', '1', '0', '1', '1', '2019-03-08 22:53:14', '2019-03-26 04:45:43'),
(2, 2, 1, '1', '1', '1', '1', '1', '0', '1', '1', '2019-03-12 01:26:39', '2019-03-16 06:37:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hdpwd` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_line_1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `address_line_2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profilepic` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `role` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `browser` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `ipaddress` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isonline` tinyint(4) DEFAULT '0',
  `llhid` int(11) DEFAULT '0',
  `number_of_locations` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_qnty` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_sum` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_emsg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `sc_ebtnclr` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sc_enote` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `unsc_emsg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `customer_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscription_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plan_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subscription_status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_period_start` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_period_end` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_exp_month` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exp_year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_on_card` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_step_1` varchar(191) DEFAULT NULL,
  `reg_step_2` varchar(191) DEFAULT NULL,
  `reg_step_3` varchar(191) DEFAULT NULL,
  `reg_step_4` varchar(191) DEFAULT NULL,
  `isprofilecomplete` varchar(191) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  `isdeleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `isapproved` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1=Approved,0=Not Approved,2=Declined',
  `isactivationcomplete` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Active,0=Inactive ',
  `logins` int(11) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_by` varchar(191) DEFAULT NULL,
  `updated_by` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `email_verified_at`, `password`, `hdpwd`, `firstname`, `lastname`, `fullname`, `phone`, `address_line_1`, `address_line_2`, `city`, `state`, `country`, `zipcode`, `profilepic`, `role`, `device`, `browser`, `ipaddress`, `active_code`, `isonline`, `llhid`, `number_of_locations`, `item_qnty`, `total_sum`, `sc_emsg`, `sc_ebtnclr`, `sc_enote`, `unsc_emsg`, `customer_id`, `subscription_id`, `plan_id`, `amount`, `subscription_status`, `current_period_start`, `current_period_end`, `start_date`, `card_number`, `card_exp_month`, `exp_year`, `name_on_card`, `reg_step_1`, `reg_step_2`, `reg_step_3`, `reg_step_4`, `isprofilecomplete`, `status`, `isdeleted`, `isapproved`, `isactivationcomplete`, `logins`, `last_login`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'reviewgrowth@gmail.com', 'reviewgrowth@gmail.com', NULL, '$2y$10$66.pRcRrBUxAWO6Y9yAHVOf29JdSUkYJpMzvGl6B/JBACwPt9AevS', '123456', 'Review', 'Growth', 'Review Growth', '9874563210', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2058163231', 'n6pjypp9jmsqeqviirsgvhvndcztdr', 1, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '1', '0', 10, '2019-12-24 13:59:36', 'ZOKw5w9iNCmmRUhLf2PqvZPyZ5Dbgo9FTKNZaiUkoSsI0yvugvhhKtJJXEMH', NULL, '1', '2019-12-18 15:49:39', '2019-12-24 13:59:36'),
(2, 'binarydata@gmail.com', 'binarydata@gmail.com', NULL, '$2y$10$TDD7kaBjHAT.lSpBmrMkueW6qNgSwviQSUKTpTm6wY8J5qVW9P7wK', '123456', 'Binary', 'Data', 'Binary Data', '', '', '', '', '', '', '', '', '2', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0', '2058163231', 'ejbnnj13434r2cawf5s3j1r48uljpv', 1, 27, '1', '0', '47', 'Thank you so much for your kind words. We really appreciate it and would love for others to read it too. Help us out by making your review go live on Google!', '#333', 'Thanks again for taking the time and effort to make sure your kind words are live on Google. It really means a lot to us and we thank you for your patronage. See you soon!', 'Thank you very much for your honest feedback. We\'re so sorry to hear that you were not 100% satisfied at our business. Customer satisfaction is so important to us and we\'d love the opportunity to improve. Please contact us during our regular business hours so we can assist you in any way. We sincerely thank you once again for your patronage. Have a wonderful day!', 'cus_GNlvMrkqOmEmIU', 'sub_GNlvo7xq4qr4Q6', 'plan_FnmzVv6VFQIgWq', '4700', 'active', '1576667263', '1579345663', '1576667263', '1111', '12', '2022', 'binarydata', '1', '1', '1', '1', '1', '1', '0', '1', '0', 11, '2019-12-24 14:00:20', NULL, '', '2', '2019-12-18 17:05:41', '2019-12-24 14:00:20'),
(3, 'advantagewebagency@gmail.com', 'advantagewebagency@gmail.com', NULL, '$2y$10$Bn.r5N7sO2vrh/AvyD.TQOSYFyakNqaMmC2WiZWcJKy6ObDyLhFiu', '123456', 'Alexander', 'Sherman', 'Alexander Sherman', '', '', '', '', '', '', '', '', '2', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', '1814637077', '0chjefw9xahcnfjqv7bcmoykpf4dvb', 1, 3, '1', '0', '47', 'Thank you so much for your kind words. We really appreciate it and would love for others to read it too. Help us out by making your review go live on Google!', '#333', 'Thanks again for taking the time and effort to make sure your kind words are live on Google. It really means a lot to us and we thank you for your patronage. See you soon!', 'Thank you very much for your honest feedback. We\'re so sorry to hear that you were not 100% satisfied at our business. Customer satisfaction is so important to us and we\'d love the opportunity to improve. Please contact us during our regular business hours so we can assist you in any way. We sincerely thank you once again for your patronage. Have a wonderful day!', 'cus_GNqGOQlAUzZ3Iv', 'sub_GNqGx5e7z0eRTO', 'plan_FnmzVv6VFQIgWq', '4700', 'active', '1576683382', '1579361782', '1576683382', '4242', '12', '2029', 'dfgdfg sftghdfgh', '1', '1', '1', '1', '1', '1', '0', '1', '1', 1, '2019-12-18 21:33:47', NULL, '', '3', '2019-12-18 21:33:47', '2019-12-18 22:12:23'),
(4, 'jbaclangen@gmail.com', 'jbaclangen@gmail.com', NULL, '$2y$10$ENsmJOSygpcvQ5UFnb3l.OBZBnqsQHRuEBpGxjMyqGE.Yt9vXQt3.', 'Support.123', 'Jeffrey', 'Baclangen', 'Jeffrey Baclangen', '', '', '', '', '', '', '', '', '2', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36', '2015186010', 'kohoviylnbouymvxam6iit7c574pjz', 1, 4, '', '', '', 'Thank you so much for your kind words. We really appreciate it and would love for others to read it too. Help us out by making your review go live on Google!', '#333', 'Thanks again for taking the time and effort to make sure your kind words are live on Google. It really means a lot to us and we thank you for your patronage. See you soon!', 'Thank you very much for your honest feedback. We\'re so sorry to hear that you were not 100% satisfied at our business. Customer satisfaction is so important to us and we\'d love the opportunity to improve. Please contact us during our regular business hours so we can assist you in any way. We sincerely thank you once again for your patronage. Have a wonderful day!', '', '', '', '', '', '', '', '', '', '', '', 'Jeffrey Baclangen', '', '', '', '', '', '1', '0', '1', '1', 1, '2019-12-18 22:14:32', NULL, '', '', '2019-12-18 22:14:32', '2019-12-18 22:15:55'),
(8, 'binarydata.marketing@gmail.com', 'binarydata.marketing@gmail.com', NULL, '$2y$10$NID/cyXz5u08kFpNzHyVUugbkYpHJut3RyL.1QVtLl5UjLSVcKcTe', 'Welcome90#@!', 'Review', 'Website', 'Review Website', '', '', '', '', '', '', '', '', '2', 'Apache/2.4.39 (Unix) mod_hive/6.27 OpenSSL/1.0.1e-fips mod_fastcgi/2.4.6', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '1969285908', 'xkbqoi7jfjy4zugohuxi4fqnsgbo66', 1, 28, '3', '2', '67', 'Thank you so much for your kind words. We really appreciate it and would love for others to read it too. Help us out by making your review go live on Google!', '#333', 'Thanks again for taking the time and effort to make sure your kind words are live on Google. It really means a lot to us and we thank you for your patronage. See you soon!', 'Thank you very much for your honest feedback. We\'re so sorry to hear that you were not 100% satisfied at our business. Customer satisfaction is so important to us and we\'d love the opportunity to improve. Please contact us during our regular business hours so we can assist you in any way. We sincerely thank you once again for your patronage. Have a wonderful day!', 'cus_GPxKtBIZhlGB01', 'sub_GPxKAkVe8YaXc4', 'plan_Fnn0ZbdDhZOAVL', '6700', 'active', '1577170951', '1579849351', '1577170951', '4242', '5', '2029', 'Review it', '1', '1', '1', '1', '1', '1', '0', '1', '1', 2, '2020-01-01 16:02:28', NULL, '', '8', '2019-12-24 12:58:27', '2020-01-01 16:02:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `businesslocations`
--
ALTER TABLE `businesslocations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `businesssetting`
--
ALTER TABLE `businesssetting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_reviews`
--
ALTER TABLE `business_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `errorlog`
--
ALTER TABLE `errorlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_reviews`
--
ALTER TABLE `live_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loginhistory`
--
ALTER TABLE `loginhistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `businesslocations`
--
ALTER TABLE `businesslocations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `businesssetting`
--
ALTER TABLE `businesssetting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `business_reviews`
--
ALTER TABLE `business_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `errorlog`
--
ALTER TABLE `errorlog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `live_reviews`
--
ALTER TABLE `live_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `loginhistory`
--
ALTER TABLE `loginhistory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
